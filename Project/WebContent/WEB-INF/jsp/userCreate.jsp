<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>ユーザー登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">ユーザー管理システム</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

			<a class="nav-link" href="#">${userInfo.name}さん</a>
            <div class="right">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </div>
        </nav>
    </header>


<form action=userCreateServlet method="post">
        <div class="main">
            <div>

                <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
                 <div class="mb-4">
                <label class="mr-3">
                    ログインID
                </label>
                <input type="text" name="loginId">
            </div>
            <div>
                <label>
                    パスワード <input type="text" name="password">
                </label>
            </div>
            <br>
            <div>
                <label>
                    パスワード（確認） <input type="text" name="password2">
                </label>
            </div>
            <br>
            <div>
                <label>
                    名前 <input type="text" name="name">
                </label>
            </div>
            <div>
                <label>
                    生年月日 <input type="date" name="date">
                </label>
            </div>
            <br>
            <input type="submit" class="btn btn-primary">
            <div>
                <a class="btn" href="UserListServlet">戻る</a>
            </div>
            <br>
        </div>
    </div>
    </form>>
</body>

</html>

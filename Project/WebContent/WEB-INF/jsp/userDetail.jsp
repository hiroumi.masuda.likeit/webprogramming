<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>ユーザー登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">ユーザー管理システム</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="nav-link" href="#">${userInfo.name}さん</a>
            <div class="right">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </div>
        </nav>
    </header>

    <div>

        <div class="main">


                <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
            <div class="mb-3">
                <label>
                    ログインID ${user.loginId}
                </label>
            </div>

            <div class="mb-3">
                <label>
                ユーザ名 ${user.name}
                </label>
            </div>

            <div class="mb-3">
                <label>
                生年月日 ${user.birthDate}
                </label>
            </div>

            <div class="mb-3">
                <label>
                新規登録日時 ${user.createDate}
                </label>
            </div>

            <div class="mb-3">
                <label>
                更新日時 ${user.updateDate}
                </label>

            <div class="mt-3">
  			  <a href="UserListServlet" class="navbar-link logout-link">戻る</a>
            </div>
            <br>
        </div>
    </div>
</body>

</html>

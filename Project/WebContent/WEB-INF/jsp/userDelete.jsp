<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>ユーザー登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">


</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">ユーザー管理システム</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            			<a class="nav-link" href="#">${userInfo.name}さん</a>

            <div class="right">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </div>
        </nav>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="main">

        <p>ログイン ID:${user.id}を消去しますか？</p>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="UserListServlet" class="btn btn-light btn-block">キャンセル</a>
                </div>
                <div class="col">
                <form action="UserDeleteServlet" method="post">
    		<input type="hidden" name="id" value="${user.id}">

                    <button type="submit" class="btn btn-primary btn-block">OK</button>
                    </form>>
                </div>
            </div>
        </div>
    </div>
</body></html>
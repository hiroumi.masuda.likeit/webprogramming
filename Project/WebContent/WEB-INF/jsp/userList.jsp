<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<title>ユーザー登録画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">ユーザー管理システム</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="nav-link" href="#">${userInfo.name}さん</a>
            <div class="right">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </div>
        </nav>
    </header>
    <br>
    <br>
    <br>
    <br>
    <div class="btn1">
        <a class="btn btn-outline-primary" href="userCreateServlet" >新規登録</a>
    </div>

   	<form action=UserListServlet method="post">

        <div>
                <label>
                    ログインID <input type="text" name="loginId">
                </label>
            </div>
            <br>
            <div>
                <label>
                   ユーザ名  <input type="text" name="name">
                </label>
            </div>

              生年月日  <input type="date" name="birthDate">〜 <input type="date" name="birthDate2">
			<button type="submit" name="検索"  style="width:100px;height:50px">検索</button>

</form>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ログインID</th>
                    <th scope="col">ユーザ名</th>
                    <th scope="col">生年月日</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
	<td>
	<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
	<c:if test="${userInfo.id==user.id }">
	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                     </c:if>

                       <c:if test="${userInfo.id==1 }">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>

                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>

                       </c:if>
                     </td>
                   </tr>
                 </c:forEach>
            </tbody>
        </table>



</body></html>

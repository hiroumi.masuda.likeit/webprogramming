<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>ユーザー登録画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">


</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">ユーザー管理システム</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

                    <a class="nav-link" href="#">ログインユーザの名前を表示</a>
        <div class="right">
                    <a class="btn btn-primary" href="index.html">ログアウト</a>
            </div>
        </nav>
    </header>

    <div class="main">

        <div>
    <div class="mb-3">
        <label class="mr-3">
            ログインID
        </label>
        <input type="text" name="名前">
        </div>
    <div class="mb-3">
        <label class="mr-3">
            パスワード
        </label>
        <input type="text" name="名前">
    </div>
    <div class="mb-3">
        <label class="mr-3">
            パスワード（確認） <input type="text" name="名前">
        </label>
    </div>
    <div class="mb-3">
        <label class="mr-3">
            生年月日 <input type="text" name="名前">
        </label>
    </div>
    <input type="submit" value="登録" class="btn btn-primary">
    <div>
        <a href="index.html">戻る</a>
    </div>
            <br>
</div>
    </div>
</body>

</html>

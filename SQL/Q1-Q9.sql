create table user
(
id SERIAL primary key,
login_id varchar(255) unique not null,
name varchar(255) not null,
birth_date DATE not null,
password varchar(255) not null,
create_date DATETIME not null,
update_date DATETIME not null
);

insert into user(login_id,name,birth_date,password,create_date,update_date)values('admin','管理者','1995-03-19','password',now(),now());